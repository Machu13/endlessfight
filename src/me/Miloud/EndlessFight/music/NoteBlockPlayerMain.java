package me.Miloud.EndlessFight.music;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.entity.Player;

public class NoteBlockPlayerMain {

	public static NoteBlockPlayerMain playerMain;
	public HashMap<String, ArrayList<SongPlayer>> playingSongs = new HashMap<String, ArrayList<SongPlayer>>();
	public HashMap<String, Byte> playerVolume = new HashMap<String, Byte>();

	public NoteBlockPlayerMain() {
		playerMain = this;
	}

	public static boolean isReceivingSong(Player p) {
		return ((playerMain.playingSongs.get(p.getName()) != null)
				&& (!playerMain.playingSongs.get(p.getName()).isEmpty()));
	}

	public static void stopPlaying(Player p) {
		if (playerMain.playingSongs.get(p.getName()) == null) {
			return;
		}
		for (SongPlayer s : playerMain.playingSongs.get(p.getName())) {
			s.removePlayer(p);
		}
	}

	public static void setPlayerVolume(Player p, byte volume) {
		playerMain.playerVolume.put(p.getName(), volume);
	}

	public static byte getPlayerVolume(Player p) {
		Byte b = playerMain.playerVolume.get(p.getName());
		if (b == null) {
			b = 100;
			playerMain.playerVolume.put(p.getName(), b);
		}
		return b;
	}
}