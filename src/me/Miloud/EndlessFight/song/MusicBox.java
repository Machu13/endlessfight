package me.Miloud.EndlessFight.song;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.Miloud.EndlessFight.EndlessFight;
import me.Miloud.EndlessFight.music.Song;

public class MusicBox implements CommandExecutor {

	private static MusicThread musicThread;

	public static void init() {

		musicThread = new MusicThread(getSongFolder());

		if (musicThread.getSongs().length == 0) {
			Bukkit.getConsoleSender().sendMessage("No songs found! Disabling plugin.");
			Bukkit.getPluginManager().disablePlugin(EndlessFight.instance);
		} else {
			Bukkit.getPluginManager().registerEvents(new MusicBoxListener(), EndlessFight.instance);
			Bukkit.getScheduler().runTaskTimer(EndlessFight.instance, musicThread, 0, 20);
		}

	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (command.getName().equals("nextsong")) {

			if (sender.hasPermission("musicbox.commands.nextsong") || sender.isOp()) {

				int times = 1;

				if (args.length > 0) {
					try {
						times = Integer.parseInt(args[0]);
					} catch (NumberFormatException e) {
						return false;
					}
				}

				getMusicThread().getSongPlayer().setPlaying(false);
				getMusicThread().nextSong(times);
				// Will cause the song to skip
				sender.sendMessage(ChatColor.GREEN + "Song flagged for skip.");
			}
			return true;

		} else if (command.getName().equals("reloadsongs")) {

			if (sender.hasPermission("musicbox.commands.reloadsongs") || sender.isOp()) {

				musicThread.getSongPlayer().setPlaying(false);

				Bukkit.getScheduler().cancelTasks(EndlessFight.instance);

				musicThread = new MusicThread(getSongFolder());

				Bukkit.getScheduler().runTaskTimer(EndlessFight.instance, musicThread, 0, 20);

				sender.sendMessage(ChatColor.GREEN + "Songs reloaded!");

			}
			return true;

		} else if (command.getName().equals("playing")) {
			if (sender.hasPermission("musicbox.commands.playing")) {

				String title = getMusicThread().getCurrentSong().getTitle();

				if (title.isEmpty()) {
					title = ChatColor.RED + "[No Name]";
				}

				sender.sendMessage(ChatColor.GREEN + "Now playing: " + title);

			}
			return true;
		} else if (command.getName().equals("songs")) {
			if (sender.hasPermission("musicbox.commands.playing")) {

				StringBuffer buf = new StringBuffer();

				buf.append(ChatColor.GREEN + "Loaded songs: ");

				Song[] songs = getMusicThread().getSongs();

				for (int i = 0; i < songs.length; i++) {

					if (i % 2 == 0) {
						buf.append(ChatColor.GRAY);
					} else {
						buf.append(ChatColor.AQUA);
					}

					buf.append(songs[i].getTitle());

					if (i < songs.length - 1) {
						buf.append(", ");
					}

				}

				sender.sendMessage(buf.toString());

			}

			return true;

		} else if (command.getName().equals("setsong")) {
			if (sender.hasPermission("musicbox.commands.setsong")) {
				if (args.length > 0) {

					String songName = "";

					for (int i = 0; i < args.length; i++) {
						songName += args[i];
						if (i < args.length - 1) {
							songName += " ";
						}
					}

					boolean success = getMusicThread().trySetSong(songName);

					if (success) {
						sender.sendMessage(ChatColor.GREEN + "Song set to " + songName);
					} else {
						sender.sendMessage(ChatColor.RED + "Song named " + songName + " does not exist");
					}

				} else {
					return false;
				}
			}

			return true;
		}

		return true;
	}

	public static MusicThread getMusicThread() {
		return musicThread;
	}

	public static File getSongFolder() {
		return new File(EndlessFight.instance.getDataFolder(), "songs/");
	}

}
