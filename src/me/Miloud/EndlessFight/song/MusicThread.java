package me.Miloud.EndlessFight.song;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.Miloud.EndlessFight.EndlessFight;
import me.Miloud.EndlessFight.music.NBSDecoder;
import me.Miloud.EndlessFight.music.RadioSongPlayer;
import me.Miloud.EndlessFight.music.Song;
import me.Miloud.EndlessFight.music.SongPlayer;
import me.Miloud.EndlessFight.utils.TitleManager;

public class MusicThread implements Runnable {

	private SongPlayer songPlayer;
	private Song[] loadedSongs;
	private int currentSong;

	public MusicThread(File songFolder) {
		currentSong = 0;
		loadedSongs = new Song[1];
		loadSongs(songFolder);
	}

	public void run() {

		if (!songPlayer.isPlaying()) {

			nextSong();

			for (Player player : Bukkit.getOnlinePlayers()) {
				songPlayer.addPlayer(player);
			}

			songPlayer.setPlaying(true);

			popupMessage();

		}

	}

	public Song getCurrentSong() {
		return getSongPlayer().getSong();
	}

	public SongPlayer getSongPlayer() {
		return songPlayer;
	}

	private void loadSongs(File songFolder) {
		File[] files = songFolder.listFiles();
		List<Song> songs = new ArrayList<Song>();

		EndlessFight.instance.getLogger().info("Loading songs from " + songFolder.getPath());

		for (File file : files) {
			Song song = null;
			try {
				song = NBSDecoder.parse(file);
			} catch (Exception e) {

				EndlessFight.instance.getLogger()
						.severe("ERROR: Failed to load song " + file.getPath() + "... " + e.getMessage());

				continue;
			}

			songs.add(song);
		}

		EndlessFight.instance.getLogger().info("Loaded " + songs.size() + " songs!");

		loadedSongs = songs.toArray(loadedSongs);

		songPlayer = new RadioSongPlayer(loadedSongs[0]);

		songPlayer.setPlaying(true);

	}

	private void popupMessage() {
		for (Player player : Bukkit.getOnlinePlayers())
			TitleManager.sendTitle(player, ChatColor.AQUA + "En cours:",
					ChatColor.GREEN + songPlayer.getSong().getTitle(), 20, 5);
	}

	public void popupMessage(Player player) {
		TitleManager.sendTitle(player, ChatColor.AQUA + "En cours:", ChatColor.GREEN + songPlayer.getSong().getTitle(),
				20, 5);
	}

	public void nextSong(int times) {
		currentSong += times;

		if (currentSong >= loadedSongs.length) {
			currentSong = 0;
		}

		songPlayer = new RadioSongPlayer(loadedSongs[currentSong]);
	}

	private void nextSong() {
		nextSong(1);

	}

	public boolean trySetSong(String songName) {

		for (Song song : loadedSongs) {
			if (song.getTitle().equalsIgnoreCase(songName)) {
				songPlayer.setPlaying(false);
				songPlayer = new RadioSongPlayer(song);
				songPlayer.setPlaying(true);
				return true;
			}
		}

		if (!songName.endsWith(".nbs")) {
			songName = songName + ".nbs";
		}

		for (Song song : loadedSongs) {
			if (song.getPath().getName().equalsIgnoreCase(songName)) {
				songPlayer.setPlaying(false);
				songPlayer = new RadioSongPlayer(song);
				songPlayer.setPlaying(true);
				return true;
			}
		}

		return false;
	}

	public Song[] getSongs() {
		return loadedSongs;
	}

}
