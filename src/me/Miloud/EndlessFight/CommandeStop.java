package me.Miloud.EndlessFight;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.Miloud.EndlessFight.utils.Cuboid;
import me.Miloud.EndlessFight.utils.TitleManager;

public class CommandeStop implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (EndlessFight.started && sender.isOp()) {
			Bukkit.getScheduler().cancelAllTasks();
			EndlessFight.started = false;
			CommandeStart.positionSongPlayer.setAutoDestroy(true);
			CommandeStart.positionSongPlayer.setPlaying(false);
			for(Player pls : Bukkit.getOnlinePlayers()){
				TitleManager.sendTitle(pls, "Fin de la partie", "Bien jou�", 20, 1);
			}
			Cuboid cuboid = new Cuboid(new Location(sender.getServer().getWorld("world"), -63,63,-63), new Location(sender.getServer().getWorld("world"), -1,84,-1));
			for(Block block : cuboid.getBlocks()){
				block.setType(Material.AIR);
			}
			
			for(Player player : Bukkit.getServer().getOnlinePlayers()){
				EndlessFight.life.put(player.getUniqueId(), 3);
			}
		} else {
			sender.sendMessage(ChatColor.RED + "Aucune partie de d�mar�.");
		}
		return false;

	}
}
