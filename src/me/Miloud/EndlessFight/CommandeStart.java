package me.Miloud.EndlessFight;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.Miloud.EndlessFight.music.NBSDecoder;
import me.Miloud.EndlessFight.music.PositionSongPlayer;
import me.Miloud.EndlessFight.music.Song;
import me.Miloud.EndlessFight.utils.TitleManager;

public class CommandeStart implements CommandExecutor {
	public static int task;
	int seconds = 10;
	int t = 3;
	public static PositionSongPlayer positionSongPlayer;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!EndlessFight.started && sender.isOp()) {
			startCountdown();
			sendSong();
		} else {
			sender.sendMessage(ChatColor.RED + "Partie d�j� d�mar�.");
		}
		return false;

	}

	private void startCountdown() {
		task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("EndlessFight"),
				new Runnable() {

					@Override
					public void run() {
						seconds--;
						for (Player pls : Bukkit.getOnlinePlayers()) {
							TitleManager.sendTitle(pls, "D�but de la partie dans " + seconds + " secondes",
									"Attention a vos tetes", 20, 1);
						}
						if (seconds == 0) {
							Bukkit.getScheduler().cancelTask(task);
							EndlessFight.starting();
						}
					}
				}, 20, 20);
	}
	
	private void sendSong(){
		for (final Player p : Bukkit.getOnlinePlayers()) {
			Song s = NBSDecoder.parse(new File(EndlessFight.instance.getDataFolder(), "/songs/OMFG-Hello.nbs"));
			positionSongPlayer = new PositionSongPlayer(s);
			positionSongPlayer.setTargetLocation(p.getEyeLocation());
			positionSongPlayer.setPlaying(true);
			positionSongPlayer.addPlayer(p);
			positionSongPlayer.setAutoDestroy(false);
			new BukkitRunnable() {
				@Override
				public void run() {
					if (p.isValid()) {
						positionSongPlayer.setTargetLocation(p.getEyeLocation());
					}
				}
			}.runTaskTimer(EndlessFight.instance, 0, 1);
		}
	}

}