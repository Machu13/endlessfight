package me.Miloud.EndlessFight;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.EntityEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import me.Miloud.EndlessFight.music.NoteBlockPlayerMain;
import me.Miloud.EndlessFight.song.MusicBox;

public class EndlessFight extends JavaPlugin {

	public static boolean started = false;
	public static Plugin instance;
	public static int task;
	public static int task2;
	public static int t = 3;
	public static HashMap<UUID, Integer> life = new HashMap<UUID, Integer>();

	@Override
	public void onEnable() {
		instance = this;
		Bukkit.getPluginManager().registerEvents(new Listeners(), this);
		getCommand("start").setExecutor(new CommandeStart());
		getCommand("stop").setExecutor(new CommandeStop());
		CommandExecutor executor = new MusicBox();
		getCommand("nextsong").setExecutor(executor);
		getCommand("reloadsongs").setExecutor(executor);
		getCommand("playing").setExecutor(executor);
		getCommand("songs").setExecutor(executor);
		getCommand("setsong").setExecutor(executor);
		Bukkit.getWorld("world").setDifficulty(Difficulty.PEACEFUL);
		for (Player player : Bukkit.getOnlinePlayers()) {
			life.put(player.getUniqueId(), 3);
		}
		new NoteBlockPlayerMain();
		MusicBox.init();
		super.onEnable();
	}

	@Override
	public void onDisable() {

		super.onDisable();
	}

	public static void starting() {
		started = true;
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.setVelocity(new Vector(0, -5, 0));
			player.teleport(new Location(player.getWorld(), -32, 63, -32));
			player.playSound(player.getLocation(), Sound.PISTON_EXTEND, 10f, 10f);
			player.playSound(player.getLocation(), Sound.CAT_HISS, 10f, 10f);
			player.playSound(player.getLocation(), Sound.PIG_IDLE, 10f, 10f);
			player.playEffect(EntityEffect.ZOMBIE_TRANSFORM);
			player.setGameMode(GameMode.ADVENTURE);
			player.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "GO !");
		}
		task = Bukkit.getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("EndlessFight"),
				new Runnable() {

					@Override
					public void run() {
						t--;
						if (t == 0) {
							Bukkit.getScheduler().cancelTask(task);
							Bukkit.getScheduler().scheduleSyncRepeatingTask(EndlessFight.instance, new Task(), 0L, 02L);
						}
					}
				}, 20 * 3, 20 * 3);
		task2 = Bukkit.getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("EndlessFight"),
				new Runnable() {

					@Override
					public void run() {
						Random random = new Random();
						int st = random.nextInt(63);
						int nd = random.nextInt(63);
						int nb = 1;
						Bukkit.getWorlds().get(0).getBlockAt(-1 - st, 63, -1 - nd).setType(Material.BEACON);
						if (Bukkit.getWorlds().get(0).getBlockAt(-1 -st, 63, -1 -nd).getType().equals(Material.AIR)) {
							Bukkit.getWorlds().get(0).getBlockAt(-1 - st, 63, -1 - nd).setType(Material.BEACON);
						}
						else{
							while (Bukkit.getWorlds().get(0).getBlockAt(-1 -st, 63+nb, -1 -nd).getType().equals(Material.AIR)) {
								if (Bukkit.getWorlds().get(0).getBlockAt(-1 -st, 63+nb, -1 -nd).getType().equals(Material.AIR)) {
									nb++;
								}
								else{
									Bukkit.getWorlds().get(0).getBlockAt(-1 - st, 63, -1 - nd).setType(Material.BEACON);
									nb = 1;
								}
							}
						}
					}
				}, 20 * 6, 20 * 6);
	}

}
