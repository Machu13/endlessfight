package me.Miloud.EndlessFight;

import java.util.Random;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.Miloud.EndlessFight.utils.TitleManager;

public class Listeners implements Listener {

	@EventHandler
	public void onHurt(EntityDamageEvent event) {
		if (event.getCause() == DamageCause.SUFFOCATION && event.getEntity() instanceof Player
				&& EndlessFight.started) {
			Player player = (Player) event.getEntity();
			player.damage(20);
		}
	}

	@EventHandler
	public static void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		player.setGameMode(GameMode.ADVENTURE);
		if (!EndlessFight.life.containsKey(player.getUniqueId())) {
			EndlessFight.life.put(player.getUniqueId(), 3);
		}
	}

	@EventHandler
	public static void onRespawn(PlayerRespawnEvent e) {
		Player player = e.getPlayer();
		if (EndlessFight.started == true) {
			e.setRespawnLocation(new Location(player.getWorld(), 6,63,-30));
			EndlessFight.life.put(player.getUniqueId(), EndlessFight.life.get(player.getUniqueId())-1);
			if (EndlessFight.life.get(player.getUniqueId()) == 0) {
				e.setRespawnLocation(new Location(player.getWorld(), 6,63,-30));
				player.setGameMode(GameMode.SPECTATOR);
				player.sendMessage("Vous n'avez plus de vie");
			}
			else{
				e.setRespawnLocation(new Location(player.getWorld(), -32,63,-32));
				player.teleport(new Location(player.getWorld(), -32,63,-32));
				player.setGameMode(GameMode.ADVENTURE);
				player.sendMessage("Il vous reste "+EndlessFight.life.get(player.getUniqueId()) + " vies");
			}
		}
	}

	@EventHandler
	public static void onInteract(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getClickedBlock().getType() == Material.BEACON) {
			Random random = new Random();
			Player player = e.getPlayer();
			e.setCancelled(true);

			e.getClickedBlock().setType(Material.AIR);
			int rd = random.nextInt(9);
			switch (rd) {
			case 0:
				TitleManager.sendTitle(player, "", "Pas de chance", 20 * 2, 1);
				break;
			case 1:
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 10, 0));
				TitleManager.sendTitle(player, "", "Un bonus : 10s de speed", 20 * 2, 1);
				break;
			case 2:
				player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 10, 1));
				TitleManager.sendTitle(player, "", "Un malus : 10s de slowness", 20 * 2, 1);
				break;
			case 3:
				player.getInventory().setItem(1, new ItemStack(Material.WOOD_SWORD));
				TitleManager.sendTitle(player, "", "Un bonus : Une �p�e en bois", 20 * 2, 1);
				break;
			case 4:
				player.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
				TitleManager.sendTitle(player, "", "Un bonus : Un casque en cuir", 20 * 2, 1);
				break;
			case 5:
				player.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
				TitleManager.sendTitle(player, "", "Un bonus : Un plastron en cuir", 20 * 2, 1);
				break;
			case 6:
				player.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
				TitleManager.sendTitle(player, "", "Un bonus : Un pantalon en cuir", 20 * 2, 1);
				break;
			case 7:
				player.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
				TitleManager.sendTitle(player, "", "Un bonus : Des chaussures en cuir", 20 * 2, 1);
				break;
			case 8:
				player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 10, 1));
				TitleManager.sendTitle(player, "", "Un malus : Ta attrap� une tumeur", 20 * 2, 1);
				break;
			}
			
		}
	}
}
